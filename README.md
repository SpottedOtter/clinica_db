# README #

Aplicação de banco de dados feita com o micro Framework de python
Flask

### Configuração básica ###
##### Basta rodar os seguintes comandos para começar a rodar a aplicação em servidor local.

1. $ cd database-app/Clinic
2. $ ./setup_venv.sh
3. $ export FLASK_APP=clinic.py; export FLASK_DEBUG=1
4. $ flask run


### Guidelines ###

* Coloque os TODOS onde faltar
* SEMPRE, antes de dar push, salve seu venv e execute ./cleaner.sh, na pasta Clinic
* Quando for dar pull, execute as instruções de configuração, caso tenha apagado o venv
