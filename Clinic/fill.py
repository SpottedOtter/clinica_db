from app import db
from datetime import datetime

from app.models import (User, Secretary, Doctor, 
                        Patient, Exam, Schedule, 
                        Appointment, RequestExam)

from passlib.hash import sha256_crypt

# Adiciona usuarios padrões para teste
password = sha256_crypt.encrypt('123')
user1 = User(login='maria', employee='SEC', password=password)
user2 = User(login='jebmaia', employee='MED', password=password)
user3 = User(login='phmendes', employee='MED', password=password)

db.session.add(user1)
db.session.add(user2)
db.session.add(user3)

db.session.commit()

# Adiciona secretária com base no login
maria = User.query.filter_by(login='maria').first()
db.session.add(Secretary(name='Maria Caroline', user_id=maria.id))

# Adiciona os medicos com base no login
ph = User.query.filter_by(login='phmendes').first()
db.session.add(Doctor(name='Paulo Henrique', user_id=ph.id,  specialty='Cardiologista', cpf='23423423423'))
jeb = User.query.filter_by(login='jebmaia').first()
db.session.add(Doctor(name='José Everardo', user_id=jeb.id, specialty='Traumatologista', cpf='34534534534'))




#Pacientes
patients = [('Humberto', 22, '12312312312'), ('Levy', 10, '23423423423'), ('Alan', 53, '45645645656')]
for pt in patients:
    print(pt[0], pt[1], pt[2])
    db.session.add(Patient(name=pt[0], age=pt[1], cpf=pt[2]))
db.session.commit()

#Agendamentos
sch1 = Schedule(secretary_id=1, doctor_id=1, patient_id=1, date=datetime(2017, 12, 12, 8), priority=1)
sch2 = Schedule(secretary_id=1, doctor_id=2, patient_id=2, date=datetime(2017, 12, 20, 9), priority=1)
sch3 = Schedule(secretary_id=1, doctor_id=1, patient_id=2, date=datetime(2017, 12, 20, 10), priority=1)
sch4 = Schedule(secretary_id=1, doctor_id=1, patient_id=3, date=datetime(2017, 12, 20, 10), priority=2)
db.session.add(sch1)
db.session.add(sch2)
db.session.add(sch3)
db.session.add(sch4)

#Consultas
cons1 = Appointment(schedule_id=1, medicines="Tylenol", symptoms="Dor de cabeça")
cons2 = Appointment(schedule_id=2, medicines="Vitamina C", symptoms="Nariz entupido")
cons3 = Appointment(schedule_id=3, medicines="Floratil", symptoms="Dor de barriga")
db.session.add(cons1)
db.session.add(cons2)
db.session.add(cons3)

#Solicitações
re1 = RequestExam(exam_id=1, appointment_id=1)
re2 = RequestExam(exam_id=2, appointment_id=2)
re3 = RequestExam(exam_id=3, appointment_id=3)
db.session.add(re1)
db.session.add(re2)
db.session.add(re3)

#Exames
exames = ["Eletrocardiograma", "Hemograma", "Radiografia", "Colonoscopia", "Calcemia"]
count = 1
for exame in exames:
    ex = Exam(name=exame)
    db.session.add(ex)
#Aplica as queries
db.session.commit()


print(Doctor.query.all())
print(Secretary.query.all())
print(Patient.query.all())
print(Schedule.query.all())
print(Appointment.query.all())
print(RequestExam.query.all())
