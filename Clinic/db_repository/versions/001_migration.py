from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
appointment = Table('appointment', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('schedule_id', Integer, nullable=False),
    Column('medicines', String(length=1023), nullable=False),
    Column('symptoms', String(length=1023), nullable=False),
)

exam = Table('exam', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=100), nullable=False),
)

request_exam = Table('request_exam', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('exam_id', Integer, nullable=False),
    Column('appointment_id', Integer, nullable=False),
)

schedule = Table('schedule', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('secretary_id', Integer, nullable=False),
    Column('doctor_id', Integer, nullable=False),
    Column('patient_id', Integer, nullable=False),
    Column('date', DateTime, nullable=False),
    Column('priority', Integer, nullable=False),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['appointment'].create()
    post_meta.tables['exam'].create()
    post_meta.tables['request_exam'].create()
    post_meta.tables['schedule'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['appointment'].drop()
    post_meta.tables['exam'].drop()
    post_meta.tables['request_exam'].drop()
    post_meta.tables['schedule'].drop()
