#!venv/bin/python3
import sys

from app import app, db
from flask import render_template, request, flash, redirect, url_for, session
from app.forms import RegisterFormDoctor, RegisterFormSecretary, DayAppointmentForm
from passlib.hash import sha256_crypt
from app.models import (
    Secretary, Doctor, User, 
    Patient, Schedule, Exam,
    Appointment, RequestExam
)
from functools import wraps
from app.models import User, Secretary, Doctor
from datetime import datetime
from .doctor_bo import DoctorBO
from .secretary_bo import Secretary_BO
from sqlalchemy import text


#
# Métodos auxiliares
#

def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Não autorizado, faça o login', 'danger')
            return redirect(url_for('login'))
    return wrap

#
# Telas padrões
#

@app.route('/index')
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

#
# Telas de operações
#

@app.route('/doctor_logged', methods=['GET', 'POST'])
def doctor_logged():
    patient_list = []
    
    if request.method == 'POST':
        option = request.form['radio']
        date = request.form['date']
        flag, dt = DoctorBO.check_appointment_date(option, date)

        if flag:
            patient_list, dt = DoctorBO.get_scheduled_patients(dt)
            if patient_list:
                return render_template('doctor_logged.html', 
                                        patient_list=patient_list,
                                        date=dt,
                                        schedule_date=dt)
            else:
                flash('Nenhum agendamento para esta data/horário', 'danger')
        else:
            flash('Selecione uma data e horário', 'danger')

    return render_template('doctor_logged.html', patient_list=patient_list)

@app.route('/secretary_logged', methods=['GET', 'POST'])
def secretary_logged():
    context = {}
    if request.method == 'GET' or request.method == 'POST' :
        doctors = Secretary_BO.get_doctors()
    
    if request.method == 'POST':
        request_tab = request.form['flag']

        if request_tab == 'schedule-query':
            context = Secretary_BO.get_schedules(request.form['date'])
            return render_template('secretary_logged.html', schedules=context)
        elif request_tab == 'patient-query':
            context = Secretary_BO.patient_report(request.form['radio'])
            return render_template('secretary_logged.html', patients=context)
        elif request_tab == 'doctor-date-query':
            context = Secretary_BO.get_gouped_doctors(
                request.form['dateInit'], request.form['dateEnd']
            )
            return render_template('secretary_logged.html', doctors=context)
        elif request_tab == 'report-query':
            context = Secretary_BO.appointment_report(request.form['CPF'])
            return render_template('secretary_logged.html', report=context)
        elif request_tab == 'remove-schedule-query':
            all_sch = Schedule.query.all()
            for sch in list(request.form):
                item = Schedule.query.filter_by(id=sch[0]).first()
                if item:
                    db.session.add(item)

                    sql = text(" ".join([
                        "update schedule", 
                        "set priority = priority - 1",
                        "where priority > {}".format(item.priority),
                        "and priority >= 0"
                    ]))
                    db.engine.execute(sql)

                    item.priority = 0
                    db.session.commit()

            flash("Agendamento removido com sucesso.", 'success')
            return redirect(url_for('secretary_logged'))

    return render_template('secretary_logged.html', context=context, all_doctors=doctors)

@app.route('/init_appointment', methods=['GET', 'POST'])
def new_appointment():
    context = {}

    if request.method == 'POST':
        request_id = request.form['flag']
        print(request.form)

        if request_id == 'new-appointment-request':

            schedule_id = request.form['radio']
            sch = Schedule.query.get(schedule_id)
            pat = Patient.query.get(sch.patient_id)
            ex = Exam.query.all()

            context['schedule'] = sch
            context['patient'] = pat
            context['exams'] = ex

            return render_template('consulta.html', context=context)

    return render_template('consulta.html', context=context)

@app.route('/doctor_history', methods=['GET', 'POST'])
def appointment_by_docotor():
    context = {}
    if request.method == 'POST':
        cpfs = []
        for item in list(request.form.keys())[:-3]:
            if item.isdigit():
                cpfs.append(request.form[item])

        context = Secretary_BO.get_doctor_appointments(
            cpfs, request.form['dateInit'], request.form['dateEnd']
        )
        print (context)

    return render_template('doctors_appointment.html', patients=context)

@app.route('/consulta')
def consulta():
    #TODO 
    #Resgatar as consultas passadas do mesmo paciente para preencher a tabela dinamicamente
    #preencher lista de exames
    #carregar os dados da consulta no form da tab "consulta atual"
    #salvar a consulta no banco de dados
    return render_template('consulta.html')

#
# Telas acesso ao sistema
#

@app.route('/logout')
@is_logged_in
def logout():
    session.clear()
    return redirect(url_for('index'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password_candidate = request.form['password']
        user_candidate = User.query.filter_by(login=username).first()
        if user_candidate is not None:
            employee = Doctor.query.filter_by(user_id=user_candidate.id).first()
            if employee is None:
                employee = Secretary.query.filter_by(user_id=user_candidate.id).first()
                if employee is None:
                    error = 'Erro no banco de dados'
                    return render_template('index.html', error=error)

            if sha256_crypt.verify(password_candidate, user_candidate.password):
                session['logged_in'] = True
                session['username'] = employee.name
                session['employee_id'] = employee.id
                if user_candidate.employee == 'MED':   
                    return redirect(url_for('doctor_logged'))
                elif user_candidate.employee == 'SEC':
                    return redirect(url_for('secretary_logged'))
            else:
                error = 'Log In inválido'
                return render_template('login.html', error=error)
        else:
            error = 'Usuário não encontrado'
            return render_template('login.html', error=error)

    return render_template('login.html')

@app.route('/new_schedule', methods=['GET', 'POST'])
def get_doctor_list():
    context = {}
    doctors = Doctor.query.all()
    if not doctors:
        flash('Nenhum médico cadastrado no banco', 'danger')
        return render_template('doctor_list.html', context=context)
    
    for i, doctor in enumerate(doctors):
        context[i] = doctor
    if(request.method == 'POST'):
        not_found = True
        doctor_cpf = request.form['doctor_cpf']
        if doctor_cpf is not None:
            doctor = Doctor.query.filter_by(cpf=doctor_cpf).first()
            flag, date = DoctorBO.check_appointment_date(request.form['radio'], request.form['patient_date'])
            if flag:
                sch = Schedule.query.filter_by(doctor_id=doctor.id, date=date).all()
                priority = 0
                if sch:
                    priority = max([p.priority for p in sch])
                priority += 1
                pat_cpf = request.form['patient_cpf']
                if pat_cpf:
                    pat = Patient.query.filter_by(cpf=pat_cpf).first()
                    if pat:
                        sch = Schedule(secretary_id=session['employee_id'],
                                    doctor_id=doctor.id,
                                    patient_id=pat.id,
                                    date=date,
                                    priority=priority)
                        db.session.add(sch)
                        db.session.commit()
                        flash('Paciente: ' + pat.name + \
                              ' agendado com sucesso! Prioridade: ' \
                              + str(priority), 'success')
                        return redirect(url_for('secretary_logged'))
                    else:
                        new_pat = Patient(
                            name=request.form['patient_name'],
                            age=request.form['patient_age'],
                            cpf=pat_cpf
                        )
                        db.session.add(new_pat)
                        db.session.commit()

                        pat = Patient.query.filter_by(cpf=pat_cpf).first()
                        sch = Schedule(secretary_id=session['employee_id'],
                                    doctor_id=doctor.id,
                                    patient_id=pat.id,
                                    date=date,
                                    priority=priority)
                        db.session.add(sch)
                        db.session.commit()

                        flash('Novo paciente cadastrado', 'info')
                        flash('Paciente: ' + pat.name + \
                              ' agendado com sucesso! Prioridade: ' \
                              + str(priority), 'success')
                        return redirect(url_for('secretary_logged'))

    return render_template('doctor_list.html', context=context)

#
# Registros de usuários
# 

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/save_appointement', methods=['GET', 'POST'])
def register_appointment():
    context = {}
    if request.method == 'POST':
        print ("ninghjgb")
        print(request.form)
        symp = request.form["symptoms"]
        med = request.form["medicines"]
        sch_id = request.form["schedule"]


        sch = Schedule.query.get(sch_id)
        sch.priority = 0
        db.session.add(sch)
        db.session.commit()

        ap = Appointment(schedule_id=sch.id, medicines=med, symptoms=symp)

        db.session.add(ap)
        db.session.commit()

        ap = Appointment.query.filter_by(schedule_id=sch.id).first()

        exams = []
        for item in request.form:
            if item == request.form[item]:
                exams.append(Exam.query.get(item[0]))

        if exams:
            reqs = []
            for exam in exams:
                req = RequestExam(exam_id=exam.id, appointment_id=ap.id)
                db.session.add(req)
                db.session.commit()
        
        flash("Exame registrado com Sucesso", "success")
        return redirect(url_for('doctor_logged'))


    return render_template('doctor_logged.html')

@app.route('/register/secretary', methods=['GET', 'POST'])
def register_secretary():
    form = RegisterFormSecretary(request.form)
    if request.method == 'POST' and form.validate():
        name = form.name.data
        username = form.username.data
        password = sha256_crypt.encrypt((str(form.password.data)))
        user = User(login=username, password=password, employee='SEC')
        db.session.add(user)
        db.session.commit()
        user = User.query.filter_by(login=username).first()
        secretary = Secretary(name=name, user_id=user.id)
        db.session.add(secretary)
        db.session.commit()
        flash('Secretária registrada com sucesso', 'success')
        return redirect(url_for('index'))
    return render_template('register_secretary.html', form=form)

@app.route('/register/doctor', methods=['GET', 'POST'])
def register_doctor():
    form = RegisterFormDoctor(request.form)
    if request.method == 'POST' and form.validate():
        name = form.name.data
        username = form.username.data
        specialty = form.specialty.data
        cpf = form.cpf.data
        password = sha256_crypt.encrypt((str(form.password.data)))
        user = User(login=username, password=password, employee='MED')
        db.session.add(user)
        db.session.commit()
        user = User.query.filter_by(login=username).first()
        doctor = Doctor(name=name, specialty=specialty, cpf=cpf, user_id=user.id)
        db.session.add(doctor)
        db.session.commit()
        flash('Médico registrado com sucesso', 'success')
        return redirect(url_for('index'))
    return render_template('register_doctor.html', form=form)

@app.route('/register_schedule', methods=['GET', 'POST'])
def register_schedule():
    
    if request.method == 'POST':
        return render_template('login.html')
    return render_template('login.html')