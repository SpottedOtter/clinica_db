import sys

from datetime import datetime
from .models import Doctor, Patient, Schedule
from flask import render_template, request, flash, redirect, url_for, session


class DoctorBO():

    @staticmethod
    def check_appointment_date(option, date):
        if option == 'radio8_9' and date != '':
            dt = list(map(int, date.split('-')))
            dt = datetime(hour=8, day=dt[2], month=dt[1], year=dt[0])
            # print(dt, file=sys.stderr)
            flag = True
        elif option == 'radio9_10' and date != '':
            dt = list(map(int, date.split('-')))
            dt = datetime(hour=9, day=dt[2], month=dt[1], year=dt[0])
            # print(dt, file=sys.stderr)
            flag = True
        elif option == 'radio10_11' and date != '':
            dt = list(map(int, date.split('-')))
            dt = datetime(hour=10, day=dt[2], month=dt[1], year=dt[0])
            # print(dt, file=sys.stderr)
            flag = True
        else:
            flag = False
            dt = None

        return flag, dt

    @staticmethod
    def get_scheduled_patients(dt):
        patient_list = []
        schedules = Schedule.query.filter_by(date=dt).all()

        if len(schedules) > 0:
            for sch in schedules:
                if sch.priority > 0:
                    patients = Patient.query.filter_by(id=sch.patient_id).all()
                    for pt in patients:
                        patient_list.append((pt, sch.priority, sch.id))

        date=str(dt)

        return patient_list, date