
from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(100), index=True, unique=True, nullable=False)
    password = db.Column(db.String(120), index=True)
    employee = db.Column(db.String(5), index=True, nullable=False)

    def __repr__(self):
        return '<User %r>' % (self.login)

class Secretary(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    name = db.Column(db.String(100), nullable=False)
    schedules = db.relationship("Schedule", backref='secretary', lazy=True)
    
    def __repr__(self):
        return '<Secretary, id = {}, user_id = {}, name = {}>'.format(
                    self.id, self.user_id, self.name
                )

class Doctor(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    name = db.Column(db.String(100), nullable=False)
    cpf = db.Column(db.String(11), nullable=False, unique=True)
    specialty = db.Column(db.String(100), nullable=False)
    schedules = db.relationship("Schedule", backref='docotor', lazy=True)    

    def __repr__(self):
        return '<Doctor, id = {}, user_id = {}, name = {}, cpf = {},\
    specialty = {}>'.format(
            self.id, self.user_id, self.name, self.cpf, self.specialty
        )

class Patient(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String(100), nullable=False)
    age = db.Column(db.Integer, nullable=False)
    cpf = db.Column(db.String(11), nullable=False, unique=True)
    schedules = db.relationship("Schedule", backref='patient', lazy=True)

    def __repr__(self):
        return '<Patient, id = {}, name = {}, age = {}, cpf = {}>'.format(
                    self.id, self.name, self.age, self.cpf
                )

class Schedule(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)

    secretary_id = db.Column(
        db.Integer, db.ForeignKey('secretary.id'), nullable=False)
    doctor_id = db.Column(
        db.Integer, db.ForeignKey('doctor.id'), nullable=False)
    patient_id = db.Column(
        db.Integer, db.ForeignKey('patient.id'), nullable=False)

    date = db.Column(db.DateTime, nullable=False)
    priority = db.Column(db.Integer, nullable=False)
    appointments = db.relationship("Appointment", backref="schedule", lazy=True)

    def __repr__(self):
        return '<Schedule, id = {}, secretary_id = {}, doctor_id = {},\
        patient_id = {}, date = {}, priority = {}>'.format(
            self.id, self.secretary_id, self.doctor_id, 
            self.patient_id, self.date, self.priority
        )

class Appointment(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    
    schedule_id = db.Column(
        db.Integer, db.ForeignKey('schedule.id'), nullable=False)

    medicines = db.Column(db.String(1023), nullable=False)
    symptoms = db.Column(db.String(1023), nullable=False)
    requests = db.relationship("RequestExam", backref="appointment", lazy=True)

    def __repr__(self):
        return '<Appointment, id = {}, schedule_id = {}, \
                medicines = {}, symptoms = {}>'.format(
                    self.id, self.schedule_id, self.medicines, self.symptoms
                )

class Exam(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String(100), nullable=False, unique=True)
    requests = db.relationship("RequestExam", backref="exam", lazy=True)

    def __repr__(self):
        return '<Exam, id = {}, name = {}>'.format(self.id, self.name)

class RequestExam(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    exam_id = db.Column(db.Integer, db.ForeignKey('exam.id'), nullable=False)

    appointment_id = db.Column(
        db.Integer, db.ForeignKey('appointment.id'), nullable=False)

    def __repr__(self):
        return '<RequestExam, id = {}, exam_id = {}, \
                appointment_id = {}>'.format(
                    self.id, self.exam_id, self.appointment_id
                )
