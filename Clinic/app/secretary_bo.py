import sys

from datetime import datetime
from .models import Doctor, Patient, Schedule, Secretary, RequestExam
from flask import request
from sqlalchemy import text
from app import db


class Secretary_BO():

    @staticmethod
    def get_schedules(date):
        schedules = []
        context = {}
        dt = list(map(int, date.split('-')))

        dt8 = datetime(hour=8, day=dt[2], month=dt[1], year=dt[0])
        dt9 = datetime(hour=9, day=dt[2], month=dt[1], year=dt[0])
        dt10 = datetime(hour=10, day=dt[2], month=dt[1], year=dt[0])

        schedules = schedules + Schedule.query.filter_by(date=dt8,).all()
        schedules = schedules + Schedule.query.filter_by(date=dt9).all()
        schedules = schedules + Schedule.query.filter_by(date=dt10).all()
        # print(schedules)

        for i, item in enumerate(schedules):
            if item.priority == 0:
                continue
            context[str(i)] = [
                Secretary.query.filter_by(id=item.secretary_id).first().name,
                Doctor.query.filter_by(id=item.doctor_id).first().name,
                Patient.query.filter_by(id=item.patient_id).first().name,
                str(item.date.time()),
                item.priority,
                item.id
            ]

        return context

    @staticmethod
    def patient_report(option):
        """ docstring """
        context = {}

        if option == 'appointments':
            sql = text(" ".join(
                [
                    "select p.name, p.cpf",
                    "from patient p, appointment a, schedule s",
                    "where p.id = s.patient_id and a.schedule_id = s.id",
                    "group by p.cpf"
                ]))

            result = db.engine.execute(sql)
        elif option == 'schedules':
            sql = text(" ".join(
                [
                    "select p.name, p.cpf",
                    "from patient p, appointment a, schedule s ",
                    "where p.id = s.patient_id and not(a.schedule_id = s.id)",
                    "group by p.cpf"
                ]))

            result = db.engine.execute(sql)
        elif option == 'all':
            result = [(p.name, p.cpf) for p in Patient.query.filter_by().all()]
        else:
            result = []

        for i, item in enumerate(result):
            context[str(i)] = item

        return context


    @staticmethod
    def get_gouped_doctors(data_init, data_end):
        """ docstring """

        context = {}
        sql1 = text(" ".join([
            "select specialty, count(*)",
            "from doctor",
            "group by specialty",
            "order by 2 desc"
        ]))

        specialty_data = db.engine.execute(sql1)

        med_data = []
        for element in list(specialty_data):
            # pega o nome de todos os médicos por especialidade por ordem descrescente
            sql2 = text(" ".join([
                "select d.name, d.specialty, count(*)",
                "from doctor d, schedule s, appointment a",
                "where d.specialty like '%{}'".format(element[0]),
                "and s.doctor_id = d.id and a.schedule_id = s.id",
                "and s.date between '{}' and '{}'".format(data_init, data_end),
                "group by d.name",
                "order by 3 desc"
            ]))

            med_data = med_data + list(db.engine.execute(sql2))

        # med_data = [list(k) for k in list(med_data)]

        # count_data = []
        # for med in med_data:
        #     sql3 = text(" ".join([
        #         "select count(*)",
        #         "from doctor d, appointment a, schedule s",
        #         "where d.id = {} and s.doctor_id = d.id".format(med[0]),
        #         "and a.schedule_id = s.id",
        #         "and s.date between '{}' and '{}'".format(data_init, data_end),
        #     ]))
        #     count_data = count_data + list(db.engine.execute(sql3))

        # for i, item in enumerate(med_data):
        #     item.append(count_data[i][0])
        #     context[str(i)] = item[1:]

        for i, item in enumerate(med_data):
            context[str(i)] = item

        return context

    @staticmethod
    def get_doctors():
        context = {}

        data = Doctor.query.filter_by().all()

        for i, item in enumerate(data):
            context[str(i)] = [
                item.name,
                item.specialty,
                item.cpf
            ]
        
        return context

    @staticmethod
    def get_doctor_appointments(cpfs, date_init, date_end):
        appointments = []
        docotors = []
        context = {}

        for cpf in cpfs:
            docotors.append(Doctor.query.filter_by(cpf=cpf).first())

        for doctor in docotors:
            sql = text(" ".join([
                "select d.name, p.name, p.cpf, s.date",
                "from patient p, appointment a, schedule s, doctor d",
                "where s.doctor_id = {}".format(doctor.id),
                "and d.name = '{}'".format(doctor.name),
                "and a.schedule_id = s.id",
                "and s.patient_id = p.id",
                "and s.date between '{}' and '{}'".format(date_init, date_end)
            ]))

            appointments = appointments + (list(db.engine.execute(sql)))

        for i, item in enumerate(appointments):
            context[str(i)] = item

        return context

    @staticmethod
    def appointment_report(cpf):
        """ docstring """
        
        context = {
        'Paciente': '',
        'Data': '',
        'Médico': '',
        'Sintomas': '',
        'Remédios': '',
        'Exames': ''
    }

        sql1 = text(" ".join([
            "select a.id, p.name, s.date, d.name, a.symptoms, a.medicines",
            "from patient p, schedule s, doctor d, appointment a",
            "where a.schedule_id = s.id and s.doctor_id = d.id and p.cpf = {}".format(cpf)
        ]))

        result = list(db.engine.execute(sql1))
        
        appointment_id = result[0][0]
        context['Paciente'] = result[0][1]
        context['Data'] = result[0][2]
        context['Médico'] = result[0][3]
        context['Sintomas'] = result[0][4]
        context['Remédios'] = result[0][5]
        
        sql2 = text(" ".join([
            "select e.name from exam e, request_exam r, appointment a",
            "where r.appointment_id = {} and r.exam_id = e.id".format(appointment_id)
        ])) 
        
        result = list(db.engine.execute(sql2))

        context['Exames'] = result[0][0]
        return context