from flask import (flash,
                   redirect,
                   url_for,
                   session,
                   logging)

from wtforms import (Form,
                     StringField,
                     IntegerField,
                     TextAreaField,
                     PasswordField,
                     validators,
                     DateTimeField)

from passlib.hash import sha256_crypt


class RegisterFormSecretary(Form):
    name = StringField('Nome', [validators.Length(min=1, max=50, message='Nome deve ter entre 1 e 50 caracteres.')])
    username = StringField('Usuário', [validators.Length(min=4, max=25, message='Usuário deve ter entre 4 e 25 caracteres.')])
    password = PasswordField('Senha', [
        validators.DataRequired(),
        validators.EqualTo('confirm', 'Senhas não conferem')
    ])
    confirm = PasswordField('Confirmar Senha')

class RegisterFormDoctor(Form):
    name = StringField('Nome', [validators.Length(min=1, max=50, message='Nome deve ter entre 1 e 50 caracteres.')])
    specialty = StringField('Especialidade', [validators.Length(min=4, max=50, message='Especialidade deve ter entre 4 e 50 caracteres.')])
    
    cpf = StringField('CPF', [
        validators.DataRequired(message='Campo obrigatório'),        
        validators.Length(min=11, max=11, message='CPF deve ter 11 caracteres.')
    ])
    
    username = StringField('Usuário', [
        validators.DataRequired(message='Campo obrigatório'),
        validators.Length(min=4, max=25, message='Usuário deve ter entre 4 e 25 caracteres.')
    ])
    
    password = PasswordField('Senha', [
        validators.DataRequired(message='Campo obrigatório'),
        validators.EqualTo('confirm', 'Senhas não conferem')
    ])
    confirm = PasswordField('Confirmar Senha')

class DayAppointmentForm(Form):
    patient_name = StringField('Nome', [validators.Length(min=1, max=50, message='Nome deve ter entre 1 e 50 caracteres.')])
    patient_cpf = StringField('CPF', [validators.Length(min=11, max=11, message='CPF deve ter 11 caracteres.')])
    patient_age = IntegerField('Idade')
    date = DateTimeField('Data')
